﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    public class Tableaux_S2 : Tableaux_K
    {
        protected override bool LineTwo()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Two";
            }

            bool lIsAValidFormula;

            if (RuleTwo_S2.AreThereUnmarkedFormulas())
            {
                RuleTwo_S2.ApplyFormulas();
                lIsAValidFormula = LineOne();
            }
            else
            {
                lIsAValidFormula = LineThree();
            }

            return lIsAValidFormula;
        }

        protected override bool LineFour()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Four";
            }

            bool lIsAValidFormula;

            if (RuleFour_S2.AreThereUnmarkedFormulas())
            {
                RuleFour_S2.ApplyFormulas();
            }

            lIsAValidFormula = LineFive();

            return lIsAValidFormula;
        }

        protected override bool LineFive()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Five";
            }

            bool lIsAValidFormula;

            if (RuleFive_S2.AreThereUnmarkedFormulas())
            {
                RuleFive_S2.ApplyFormulas();
            }

            lIsAValidFormula = LineSix();

            return lIsAValidFormula;
        }
    }
}
