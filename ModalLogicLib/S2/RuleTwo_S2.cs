﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleTwo_S2
    {
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    Lexer lLexer = new Lexer();
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                    {
                        ///Is this a function applied on an attomic proposition?
                        if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                        {
                            string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);

                            bool lIsANot = lSecondaryFormulaName == Lexer.FUNCTION_NOT.Name;
                            bool lIsAnOr = lSecondaryFormulaName == Lexer.FUNCTION_OR.Name;
                            bool lIsAnImplies = lSecondaryFormulaName == Lexer.FUNCTION_IMPLIES.Name;

                            ///Check for ~~A, ~(A v B), ~(A -> B), 
                            if (lIsANot || lIsAnOr || lIsAnImplies)
                            {
                                lAreThereUnmarkedFormulas = true;
                                break;
                            }
                        }
                    }
                    else if (lPrimaryFormulaName == Lexer.FUNCTION_AND.Name)
                    {
                        lAreThereUnmarkedFormulas = true;
                        break;
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        public static void ApplyFormulas()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                    {
                        ///Is this a function applied on an attomic proposition?
                        if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                        {
                            string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                            List<string> lSecondaryFormulaParamenters = lLexer.GetFunctionParameters(lPrimaryFormulaParamenters[0]);

                            if (lSecondaryFormulaName == Lexer.FUNCTION_NOT.Name)            ///Check for ~~A
                            {
                                Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lSecondaryFormulaParamenters[0]);

                                ///The node was used
                                Node.Nodes[i].Used = true;
                            }
                            else if (lSecondaryFormulaName == Lexer.FUNCTION_OR.Name)       /// ~(A v B)
                            {
                                foreach (string tFormula in lSecondaryFormulaParamenters)
                                {
                                    string tNewFormula = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, tFormula);
                                    Node.Nodes[i].AddChilds(Node.Nodes[i].Label, tNewFormula);
                                }

                                ///The node was used
                                Node.Nodes[i].Used = true;
                            }
                            else if (lSecondaryFormulaName == Lexer.FUNCTION_IMPLIES.Name)       /// ~(A -> B)
                            {
                                string lSecondFormula = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lSecondaryFormulaParamenters[1]);

                                Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lSecondaryFormulaParamenters[0]);
                                Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lSecondFormula);

                                ///The node was used
                                Node.Nodes[i].Used = true;
                            }
                        }
                    }
                    else if (lPrimaryFormulaName == Lexer.FUNCTION_AND.Name)// (A ^ B)
                    {
                        foreach (string tFormula in lPrimaryFormulaParamenters)
                        {
                            Node.Nodes[i].AddChilds(Node.Nodes[i].Label, tFormula);
                        }

                        ///The node was used
                        Node.Nodes[i].Used = true;
                    }
                }
            }
        }
    }
}