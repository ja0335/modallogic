﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleFive_S2
    {
        /// <summary>
        /// Checks for <>A and ~[]A
        /// </summary>
        /// <returns></returns>
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    ///Check for <>A and if world is normalized
                    if (lPrimaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name && IsWorldNormal(Node.Nodes[i].Label))
                    {
                        /// Check at least one branch is not closed
                        if (Node.Nodes[i].HasOpenLeafs())
                        {
                            lAreThereUnmarkedFormulas = true;
                            break;
                        }
                    }
                    else if (Node.Nodes[i].Formula.Length > 1) ///Check for ~[]A
                    {
                        ///formula is not attomic then proceed
                        List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                        if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                        {
                            ///Is this a function applied on an attomic proposition?
                            if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                            {
                                string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                                bool lIsANecessary = lSecondaryFormulaName == Lexer.FUNCTION_NECESSARY.Name;

                                ///Check for ~[]A
                                if (lIsANecessary && IsWorldNormal(Node.Nodes[i].Label))
                                {
                                    /// Check at least one branch is not closed
                                    if (Node.Nodes[i].HasOpenLeafs())
                                    {
                                        lAreThereUnmarkedFormulas = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        /// <summary>
        /// - If world is 1, then is normal
        /// - If in world exists a formula in the form []A or ~<>A, then world is normal
        /// </summary>
        /// <param name="pWorld"></param>
        /// <returns></returns>
        private static bool IsWorldNormal(int pWorld)
        {
            bool lIsNormal = false;

            if (pWorld == 1)
            {
                lIsNormal = true;
            }
            else
            {
                int lNodesCount = Node.Nodes.Count;
                Lexer lLexer = new Lexer();

                for (int i = 0; i < lNodesCount; i++)
                {
                    if (Node.Nodes[i].Label == pWorld && Node.Nodes[i].Formula.Length > 1)
                    {
                        string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                        if (lPrimaryFormulaName == Lexer.FUNCTION_NECESSARY.Name) ///Check for []A
                        {
                            lIsNormal = true;
                        }
                        else if (Node.Nodes[i].Formula.Length > 1) ///Check for ~<>A
                        {
                            ///formula is not attomic then proceed
                            List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                            if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                            {
                                ///Is this a function applied on an attomic proposition?
                                if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                                {
                                    string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                                    bool lIsAPossible = lSecondaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name;

                                    ///Check for ~<>A
                                    if (lIsAPossible)
                                        lIsNormal = true;
                                }
                            }
                        }
                    }
                }
            }

            return lIsNormal;
        }

        /// <summary>
        /// Apply rules for <>A and ~[]A
        /// </summary>
        /// <returns></returns>
        public static void ApplyFormulas()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    //Check for <> A and if world is normalized
                    if (lPrimaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name && IsWorldNormal(Node.Nodes[i].Label))
                    {
                        /// Check at least one branch is not closed
                        if (Node.Nodes[i].HasOpenLeafs())
                        {
                            List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);
                            int lNewLabel = Node.GetLastWorld();
                            Node.Nodes[i].AddChilds(lNewLabel, lFormulaParameters[0]);

                            ///The node was used
                            Node.Nodes[i].Used = true;

                            ///The steps requires the rule to applied just for one formula. So we find one and then break
                            break;
                        }
                    }
                    else if (Node.Nodes[i].Formula.Length > 1) ///Check for ~[]A
                    {
                        ///formula is not attomic then proceed
                        List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                        if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                        {
                            ///Is this a function applied on an attomic proposition?
                            if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                            {
                                string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                                bool lIsANecessary = lSecondaryFormulaName == Lexer.FUNCTION_NECESSARY.Name;

                                ///Check for ~[]A
                                if (lIsANecessary && IsWorldNormal(Node.Nodes[i].Label))
                                {
                                    /// Check at least one branch is not closed
                                    if (Node.Nodes[i].HasOpenLeafs())
                                    {
                                        List<string> lSecondaryFormulaParamenters = lLexer.GetFunctionParameters(lPrimaryFormulaParamenters[0]);

                                        //Apply ~A
                                        string lSecondFormula = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lSecondaryFormulaParamenters[0]);

                                        //Add the new node
                                        int lNewLabel = Node.GetLastWorld();
                                        Node.Nodes[i].AddChilds(lNewLabel, lSecondFormula);


                                        ///The node was used
                                        Node.Nodes[i].Used = true;

                                        ///The steps requires the rule to applied just for one formula. So we find one and then break
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
