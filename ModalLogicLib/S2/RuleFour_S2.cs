﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleFour_S2
    {
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                if (Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NECESSARY.Name) ///Check for []A
                    {
                        lAreThereUnmarkedFormulas = true;
                    }
                    else if (Node.Nodes[i].Formula.Length > 1) ///Check for ~<>A
                    {
                        ///formula is not attomic then proceed
                        List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                        if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                        { 
                            ///Is this a function applied on an attomic proposition?
                            if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                            {
                                string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                                bool lIsAPossible = lSecondaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name;

                                ///Check for ~<>A
                                if (lIsAPossible)
                                    lAreThereUnmarkedFormulas = true;
                            }
                        }
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        private static void AppplyNecessaryFormula()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                if (Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<int> lChildWorlds = Node.GetChildWorlds(Node.Nodes[i].Label);

                    /// We need here the formula to be applied in the world containing this formula
                    lChildWorlds.Add(Node.Nodes[i].Label);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NECESSARY.Name && lChildWorlds.Count > 0)
                    {
                        List<Node> lLeafs = Node.Nodes[i].GetLeafs();
                        List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                        foreach (var tLeaf in lLeafs)
                        {
                            if (!tLeaf.Closed)
                            {
                                List<int> lChildWorldsToNotApplyFormula = new List<int>();
                                ///The algorithm requires j: P, doesn't occurr in the child worlds present in this branch, so here we check it
                                Node lNode = tLeaf;

                                while (lNode != null)
                                {
                                    if (lNode.Formula == lFormulaParameters[0] && lChildWorlds.Contains(lNode.Label))
                                        lChildWorldsToNotApplyFormula.Add(lNode.Label);

                                    lNode = lNode.Parent;
                                }

                                ///Apply the formula in the worlds not present in the lChildWorldsToNotApplyFormula
                                foreach (var tChildWorld in lChildWorlds)
                                {
                                    if (!lChildWorldsToNotApplyFormula.Contains(tChildWorld))
                                        tLeaf.AddChilds(tChildWorld, lFormulaParameters[0], string.Empty, Node.Nodes[i]);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void AppplyNotPossibleFormula()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                if (Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                    {
                        ///formula is not attomic then proceed
                        List<string> lPrimaryFormulaParamenters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                        ///Is this a function applied on an attomic proposition?
                        if (lPrimaryFormulaParamenters.Count == 1 && lPrimaryFormulaParamenters[0].Length > 1)
                        {
                            string lSecondaryFormulaName = lLexer.GetFunctionName(lPrimaryFormulaParamenters[0]);
                            bool lIsAPossible = lSecondaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name;

                            ///Check for ~<>A
                            if (lIsAPossible)
                            {
                                List<int> lChildWorlds = Node.GetChildWorlds(Node.Nodes[i].Label);

                                /// We need here the formula to be applied in the world containing this formula
                                lChildWorlds.Add(Node.Nodes[i].Label);
                                List<Node> lLeafs = Node.Nodes[i].GetLeafs();
                                
                                /// Get the secondary formula parameters (<>A)
                                List<string> lSecondaryFormulaParameters = lLexer.GetFunctionParameters(lPrimaryFormulaParamenters[0]);

                                /// We will add the formula negation, so negate it
                                lSecondaryFormulaParameters[0] = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lSecondaryFormulaParameters[0]);

                                foreach (var tLeaf in lLeafs)
                                {
                                    if (!tLeaf.Closed)
                                    {
                                        List<int> lChildWorldsToNotApplyFormula = new List<int>();
                                        ///The algorithm requires j: P, doesn't occurr in the child worlds present in this branch, so here we check it
                                        Node lNode = tLeaf;

                                        while (lNode != null)
                                        {
                                            if (lNode.Formula == lSecondaryFormulaParameters[0] && lChildWorlds.Contains(lNode.Label))
                                                lChildWorldsToNotApplyFormula.Add(lNode.Label);

                                            lNode = lNode.Parent;
                                        }

                                        ///Apply the formula in the worlds not present in the lChildWorldsToNotApplyFormula
                                        foreach (var tChildWorld in lChildWorlds)
                                        {
                                            if (!lChildWorldsToNotApplyFormula.Contains(tChildWorld))
                                                tLeaf.AddChilds(tChildWorld, lSecondaryFormulaParameters[0], string.Empty, Node.Nodes[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void ApplyFormulas()
        {
            AppplyNecessaryFormula();
            AppplyNotPossibleFormula();
        }
    }
}
