﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleFive_K
    {
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name)
                    {
                        /// Check at least one branch is not closed
                        if( Node.Nodes[i].HasOpenLeafs())
                        {
                            lAreThereUnmarkedFormulas = true;
                            break;
                        }
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        public static void ApplyFormulas()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_POSSIBLE.Name)
                    {
                        /// Check at least one branch is not closed
                        if (!Node.Nodes[i].HasOpenLeafs())
                            continue;

                        List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);
                        int lNewLabel = Node.GetLastWorld();
                        
                        Node.Nodes[i].AddChilds(lNewLabel, lFormulaParameters[0]);

                        ///The node was used
                        Node.Nodes[i].Used = true;

                        ///The steps requires the rule to applied just for one formula. So we find one and then break
                        break;
                    }
                }
            }
        }
    }
}
