﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleThree_K
    {
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                    if (lFormulaParameters.Count == 2)
                    {
                        if (lPrimaryFormulaName == Lexer.FUNCTION_OR.Name) /// A v B
                        {
                            lAreThereUnmarkedFormulas = true;
                            break;
                        }
                        else if (lPrimaryFormulaName == Lexer.FUNCTION_IMPLIES.Name) /// A => B
                        {
                            lAreThereUnmarkedFormulas = true;
                            break;
                        }
                    }
                    else if (lFormulaParameters.Count == 1) ///Check for ~(A ^ B)
                    {
                        if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name) 
                        {
                            if (lLexer.GetFunctionName(lFormulaParameters[0]) == Lexer.FUNCTION_AND.Name)
                            {
                                lAreThereUnmarkedFormulas = true;
                                break;
                            }
                        }
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        public static void ApplyFormulas()
        {
            int lNodesCount = Node.Nodes.Count;

            for (int i = 0; i < lNodesCount; i++)
            {
                ///If The node is not marked & it's formula is not attomic then proceed
                if (!Node.Nodes[i].Used && Node.Nodes[i].Formula.Length > 1)
                {
                    Lexer lLexer = new Lexer();
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);

                    if (lFormulaParameters.Count == 2)
                    {
                        if (lPrimaryFormulaName == Lexer.FUNCTION_OR.Name) ///Check for A v B
                        {
                            ///Here we just add the A, B propositions
                            Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lFormulaParameters[0], lFormulaParameters[1]);

                            ///The node was used
                            Node.Nodes[i].Used = true;
                        }
                        else if (lPrimaryFormulaName == Lexer.FUNCTION_IMPLIES.Name) ///Check for A -> B
                        {
                            /// Deny the first proposition
                            lFormulaParameters[0] = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lFormulaParameters[0]);

                            Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lFormulaParameters[0], lFormulaParameters[1]);

                            ///The node was used
                            Node.Nodes[i].Used = true;
                        }
                    }
                    else if (lFormulaParameters.Count == 1)
                    {
                        if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name) ///Check for ~(A ^ B)
                        {
                            if (lLexer.GetFunctionName(lFormulaParameters[0]) == Lexer.FUNCTION_AND.Name)
                            {
                                ///Here we get the A, B propositions
                                List<string> lSecondaryFormulaParameters = lLexer.GetFunctionParameters(lFormulaParameters[0]);

                                lSecondaryFormulaParameters[0] = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lSecondaryFormulaParameters[0]);
                                lSecondaryFormulaParameters[1] = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, lSecondaryFormulaParameters[1]);

                                Node.Nodes[i].AddChilds(Node.Nodes[i].Label, lSecondaryFormulaParameters[0], lSecondaryFormulaParameters[1]);

                                ///The node was used
                                Node.Nodes[i].Used = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
