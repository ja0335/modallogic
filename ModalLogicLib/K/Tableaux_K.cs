﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    public class Tableaux_K
    {
        #region Private/Protected Fields

        protected Lexer _Lexer;
        
        protected bool _HasTreeChanged;

        protected bool _Debug = false;

        protected int _CurrentStepValue = 0;

        protected string _CurrentLineMsg = "";

        protected static int _StepValue = 2;
        
        #endregion

        #region Properties

        public Lexer Lexer
        {
            get { return _Lexer; }
        }

        #endregion

        #region Constructor

        public Tableaux_K()
        {
            _Lexer = new Lexer();
            _HasTreeChanged = false;
        }

        #endregion

        #region Tableaux Algorithm Steps

        private bool Start(string pFormula, out string stepByStep)
        {
            bool lIsAValidFormula = false;

            pFormula = _Lexer.NormalizeString(pFormula);

            Node root = new Node(1, _Lexer.ApplyFunction(Lexer.FUNCTION_NOT, pFormula));

            lIsAValidFormula = LineOne();

            ///Here we obtain the step by step logic used to reach the conclusion
            stepByStep = root.ToString();

            return lIsAValidFormula;
        }

        protected virtual bool LineOne()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";    
                    
                _CurrentLineMsg += "Line One";
            }

            bool lIsAValidFormula;
            Node.HasTreeChanged = false;

            RuleOne_K.CloseBranches();

            if (RuleOne_K.AreAllBranchesClosed())
            {
                lIsAValidFormula = true;
            }
            else
            {
                lIsAValidFormula = LineTwo();
            }

            return lIsAValidFormula;
        }

        protected virtual bool LineTwo()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Two";
            }

            bool lIsAValidFormula;

            if (RuleTwo_K.AreThereUnmarkedFormulas())
            {
                RuleTwo_K.ApplyFormulas();
                lIsAValidFormula = LineOne();
            }
            else
            {
                lIsAValidFormula = LineThree();
            }

            return lIsAValidFormula;
        }

        protected virtual bool LineThree()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Three";
            }

            bool lIsAValidFormula;

            if (RuleThree_K.AreThereUnmarkedFormulas())
            {
                RuleThree_K.ApplyFormulas();
                lIsAValidFormula = LineOne();
            }
            else
            {
                lIsAValidFormula = LineFour();
            }

            return lIsAValidFormula;
        }

        protected virtual bool LineFour()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Four";
            }

            bool lIsAValidFormula;

            if (RuleFour_K.AreThereUnmarkedFormulas())
            {
                RuleFour_K.ApplyFormulas();
            }

            lIsAValidFormula = LineFive();

            return lIsAValidFormula;
        }

        protected virtual bool LineFive()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Five";
            }

            bool lIsAValidFormula;

            if (RuleFive_K.AreThereUnmarkedFormulas())
            {
                RuleFive_K.ApplyFormulas();
            }

            lIsAValidFormula = LineSix();

            return lIsAValidFormula;
        }

        protected virtual bool LineSix()
        {
            if (_Debug)
            {
                if (_CurrentStepValue < _StepValue)
                    _CurrentStepValue += 1;
                else
                    return false;

                if (_CurrentLineMsg != string.Empty)
                    _CurrentLineMsg += " -> ";

                _CurrentLineMsg += "Line Six";
            }

            bool lIsAValidFormula;

            if (Node.HasTreeChanged)
            {
                lIsAValidFormula = LineOne();
            }
            else
            {
                lIsAValidFormula = false;
            }

            return lIsAValidFormula;
        }

        #endregion

        #region Utility Functions

        private bool IsNodeFormulaAtomic(Node pNode)
        {
            return pNode.Formula.Length == 1;
        }

        private bool IsFormulaAtomic(string pFormula)
        {
            return pFormula.Length == 1;
        }

        #endregion

        public string Step(string pTheorem)
        {
            _Debug = true;
            _CurrentStepValue = 0;
            _CurrentLineMsg = string.Empty;
            string lStepByStep;

            Start(pTheorem, out lStepByStep);

            _StepValue += 1;

            _Debug = false;

            return _CurrentLineMsg;
        }

        public void ResetStepByStep()
        {
            _StepValue = 2;
            _CurrentStepValue = 0;
            _CurrentLineMsg = string.Empty;
            Node.Clear();
        }

        public bool IsTheorem(string pTheorem, out string pStepByStep)
        {
            bool lResult = true;
            ResetStepByStep();

            lResult = Start(pTheorem, out pStepByStep);

            return lResult;
        }
    }
}
