﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    public class RuleOne_K
    {
        public static void CloseBranches()
        {
            int lNodesCount = Node.Nodes.Count;

            for (int i = 0; i < lNodesCount; i++)
            {
                ///First search for the leafs in the tree, i.e, nodes with no childs
                if (Node.Nodes[i].Closed == false && Node.Nodes[i].Left == null && Node.Nodes[i].Right == null)
                {
                    ///It is a leaf so we need check for this branch if there 
                    ///is simple well formed formulas  and its negation
                    CheckBranchClousure(i);
                }
            }
        }

        private static void CheckBranchClousure(int pIndex)
        {
            string lFormulaNegation;
            Node lNode = Node.Nodes[pIndex];
            int lLabel = Node.Nodes[pIndex].Label;
            
            while (lNode != null && lLabel == lNode.Label)
            {
                ///Is this node simple well formed? AND
                ///Does the negation for this simple well formed formula exists in the branch?
                if (IsFormulaSimpleWellFormed(lNode, out lFormulaNegation) && SearchFormulaUpwards(lNode, lFormulaNegation))
                {
                    ///We have found the neccessary conditions to close the branch
                    Node.Nodes[pIndex].Closed = true;

                    break;
                }

                lNode = lNode.Parent;

                if (lNode != null)
                    lLabel = lNode.Label;
                else
                    break;
            }
        }

        /// <summary>
        /// Takes the formula in the Node, and looks if it is simple well formed (swf)
        /// i.e. the formula is atomic, for example 
        ///         A, NOT(A), 
        /// are swf formulas
        /// If the algorithm founds it is a swf, then it sets the negation for the formula
        /// in the given string pFormulaNegation
        /// </summary>
        /// <param name="pNode"></param>
        /// <param name="pFormulaNegation"></param>
        /// <returns></returns>
        private static bool IsFormulaSimpleWellFormed(Node pNode, out string pFormulaNegation)
        {
            Lexer lLexer = new Lexer();
            pFormulaNegation = string.Empty;
            bool lResult = false;

            /// it is attomic so we just need to find its negation
            if (pNode.Formula.Length == 1)
            {
                pFormulaNegation = lLexer.ApplyFunction(Lexer.FUNCTION_NOT, pNode.Formula);
                lResult = true;
            }
            else
            {
                /// It is not attomic probably it is a formula of the type NOT(formula), 
                /// if it is the case then the negation is just the formula inside the NOT
                string lPrimaryFormulaName = lLexer.GetFunctionName(pNode.Formula);

                if (lPrimaryFormulaName == Lexer.FUNCTION_NOT.Name)
                {
                    List<string> lFormulaParameters = lLexer.GetFunctionParameters(pNode.Formula);

                    ///The formula parameters must be atomic
                    if (lFormulaParameters[0].Length == 1)
                    {
                        pFormulaNegation = lFormulaParameters[0];
                        lResult = true;
                    }
                }
            }

            return lResult;
        }

        /// <summary>
        /// Given a non inclusive search start node and a formula the
        /// algorithm will search up the branch if the given formula exist 
        /// </summary>
        /// <param name="pNode"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        private static bool SearchFormulaUpwards(Node pNode, string pFormula)
        {
            Node lNode = pNode.Parent;
            int lLabel = pNode.Label;

            while (lNode != null)
            {
                if (lNode.Formula == pFormula && lLabel == lNode.Label)
                    return true;
                else
                    lNode = lNode.Parent;
            }

            return false;
        }
        
        public static bool AreAllBranchesClosed()
        {
            bool lAreAllBranchesClosed = true;
            int lNodesCount = Node.Nodes.Count;

            for (int i = 0; i < lNodesCount; i++)
            {
                ///First search for the leafs in the tree, i.e, nodes with no childs
                if (Node.Nodes[i].Left == null && Node.Nodes[i].Right == null)
                {
                    if (Node.Nodes[i].Closed == false)
                    {
                        lAreAllBranchesClosed = false;
                        break;
                    }
                }
            }

            return lAreAllBranchesClosed;
        }
    }
}
