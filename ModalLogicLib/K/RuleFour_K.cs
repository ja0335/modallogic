﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    class RuleFour_K
    {
        public static bool AreThereUnmarkedFormulas()
        {
            bool lAreThereUnmarkedFormulas = false;
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                if (Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NECESSARY.Name)
                    {
                        lAreThereUnmarkedFormulas = true;
                    }
                }
            }

            return lAreThereUnmarkedFormulas;
        }

        public static void ApplyFormulas()
        {
            int lNodesCount = Node.Nodes.Count;
            Lexer lLexer = new Lexer();

            for (int i = 0; i < lNodesCount; i++)
            {
                if (Node.Nodes[i].Formula.Length > 1)
                {
                    string lPrimaryFormulaName = lLexer.GetFunctionName(Node.Nodes[i].Formula);
                    List<int> lChildWorlds = Node.GetChildWorlds(Node.Nodes[i].Label);

                    if (lPrimaryFormulaName == Lexer.FUNCTION_NECESSARY.Name && lChildWorlds.Count > 0)
                    {
                        List<Node> lLeafs = Node.Nodes[i].GetLeafs();
                        List<string> lFormulaParameters = lLexer.GetFunctionParameters(Node.Nodes[i].Formula);
                        
                        foreach (var tLeaf in lLeafs)
                        {
                            if (!tLeaf.Closed)
                            {
                                List<int> lChildWorldsToNotApplyFormula = new List<int>();
                                ///The algorithm requires j: P, doesn't occurr in the child worlds present in this branch, so here we check it
                                Node lNode = tLeaf;

                                while (lNode != null)
                                {
                                    if (lNode.Formula == lFormulaParameters[0] && lChildWorlds.Contains(lNode.Label))
                                        lChildWorldsToNotApplyFormula.Add(lNode.Label);

                                    lNode = lNode.Parent;
                                }
                                                                
                                ///Apply the formula in the worlds not present in the lChildWorldsToNotApplyFormula
                                foreach (var tChildWorld in lChildWorlds)
                                {
                                    if (!lChildWorldsToNotApplyFormula.Contains(tChildWorld))
                                        tLeaf.AddChilds(tChildWorld, lFormulaParameters[0], string.Empty, Node.Nodes[i]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
