﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModalLogicLib
{
    public class Node
    {
        #region Static members

        /// <summary>
        /// Just an identifier for each node
        /// </summary>
        public static int IndexCounter = 1;
        
        /// <summary>
        /// The key is the world indicator and the value is the parent indicator
        /// </summary>
        public static Dictionary<int, int> Worlds;

        /// <summary>
        /// A simple access unordered list for the current theorem
        /// </summary>
        public static List<Node> Nodes;

        public static bool HasTreeChanged = false;

        #endregion

        #region Private/Protected Fields

        private bool _Closed;

        private Node _Left;

        private Node _Right;
        
        #endregion

        #region Properties

        public int Index
        {
            get;
            set;
        }

        public bool Closed
        {
            get { return _Closed; }
            set
            {
                if (value)
                    HasTreeChanged = true;

                _Closed = value;
            }
        }

        public bool Used
        {
            get;
            set;
        }

        public int Label
        {
            get;
            set;
        }

        public string Formula
        {
            get;
            set;
        }

        public Node Parent
        {
            get;
            set;
        }

        public Node InferedBy
        {
            get;
            set;
        }

        public Node Left
        {
            get { return _Left; }
            set
            {
                _Left = value;
                _Left.Parent = this;
            }
        }

        public Node Right
        {
            get { return _Right; }
            set
            {
                _Right = value;
                _Right.Parent = this;
            }
        }

        #endregion

        #region Logic

        public Node(int pLabel, string pFormula, Node pParent = null, int pParentLabel = int.MinValue, Node pInferedBy = null)
        {
            Closed = false;
            Used = false;
            Label = pLabel;
            Formula = pFormula;
            Parent = pParent;
            InferedBy = pInferedBy;

            if (pParent == null)
            {
                IndexCounter = 1;
                Index = IndexCounter++;
                Clear();
                Nodes.Add(this);
            }
            else
            {
                Index = IndexCounter++;
            }

            if (!Worlds.ContainsKey(pLabel))
            {
                int lParentLabel = 0;

                if (pParentLabel != int.MinValue)
                    lParentLabel = pParentLabel;

                if(pInferedBy == null)
                    Worlds.Add(pLabel, pParentLabel);
                else
                    Worlds.Add(pLabel, pInferedBy.Label);
            }
        }

        public static void Clear()
        {
            Nodes = new List<Node>();
            Worlds = new Dictionary<int, int>();
        }

        public static List<int> GetChildWorlds(int pWorld)
        {
            List<int> lChildWorlds = new List<int>();

            foreach (var entry in Worlds)
            {
                if (entry.Value == pWorld)
                    lChildWorlds.Add(entry.Key);
            }

            return lChildWorlds;
        }

        public static int GetLastWorld()
        {
            int lLastWorld = Worlds.Count + 1;

            return lLastWorld;
        }
        
        public void AddChilds(int pLabel, String pLeftFormula, String pRightFormula = "", Node pInferedBy = null)
        {
            int lNodesCount = Nodes.Count;
            
            for (int i = 0; i < lNodesCount; i++)
            {
                /// First search for the leafs in the tree, i.e, nodes with no childs
                /// those leafs must not closed
                if (Nodes[i].Left == null && Nodes[i].Right == null && !Nodes[i].Closed)
                {
                    Node lNode = Nodes[i];
                    bool lIsChild = false;

                    ///Travel up wards the leaf to see if it is child of this node
                    while (lNode != null)
                    {
                        if (lNode == this)
                        {
                            lIsChild = true;
                            break;
                        }

                        lNode = lNode.Parent;
                    }

                    ///The current node is a child of this node so add the given nodes
                    if (lIsChild)
                    {
                        if (pLeftFormula != String.Empty)
                        {
                            if(pInferedBy == null)
                                Nodes[i].Left = new Node(pLabel, pLeftFormula, Nodes[i], Nodes[i].Label, this);
                            else
                                Nodes[i].Left = new Node(pLabel, pLeftFormula, Nodes[i], Nodes[i].Label, pInferedBy);

                            Nodes.Add(Nodes[i].Left);
                            HasTreeChanged = true;
                        }

                        if (pRightFormula != String.Empty)
                        {
                            if(pInferedBy == null)
                                Nodes[i].Right = new Node(pLabel, pRightFormula, Nodes[i], Nodes[i].Label, this);
                            else
                                Nodes[i].Right = new Node(pLabel, pRightFormula, Nodes[i], Nodes[i].Label, pInferedBy);

                            Nodes.Add(Nodes[i].Right);
                            HasTreeChanged = true;
                        }
                    }
                }
            }
        }

        public List<Node> GetLeafs()
        {
            List<Node> lLeafs = new List<Node>();
            int lNodesCount = Nodes.Count;

            for (int i = 0; i < lNodesCount; i++)
            {
                ///First search for the leafs in the tree, i.e, nodes with no childs
                if (Nodes[i].Left == null && Nodes[i].Right == null)
                {
                    Node lNode = Nodes[i];
                    bool lIsChild = false;

                    ///Travel up wards the leaf to see if it is child of this node
                    while (lNode != null)
                    {
                        if (lNode == this)
                        {
                            lIsChild = true;
                            break;
                        }

                        lNode = lNode.Parent;
                    }

                    ///The current node is a child of this node so add to the list
                    if (lIsChild)
                        lLeafs.Add(Nodes[i]);
                }
            }

            return lLeafs;
        }

        public bool HasOpenLeafs()
        {
            List<Node> lLeafs = GetLeafs();

            foreach (Node tNode in lLeafs)
            {
                if (!tNode.Closed)
                {
                    return true;
                }
            }

            return false;
        }

        private int ParentCount()
        {
            int lParentCount = 0;
            Node lParent = Parent;

            while (lParent != null)
            {
                lParent = lParent.Parent;
                lParentCount++;
            }

            return lParentCount;
        }

        private string TabIdentation()
        {
            string lTabIdentation = "";
            int lParentCount = ParentCount();

            for (int i = 0; i < lParentCount; i++)
                lTabIdentation += "\t";

            return lTabIdentation;
        }

        public override string ToString()
        {
            string lString = "";

            if (Used)
                lString += "Used: ";

            lString += "[" + Index.ToString() + "]. " + Label.ToString() + ": " + Formula;

            if (InferedBy != null)
                lString += "\t" + "by: [" + InferedBy.Index.ToString() + "]";

            if (Closed)
                lString += " X";

            lString += "\n";

            if (Left != null)
                lString += TabIdentation() + Left.ToString() + "";
            if (Right != null)
                lString += TabIdentation() + Right.ToString() + "";

            lString += "";

            return lString;
        }

        public string GetSymbolicString()
        {
            Lexer lLexer = new Lexer();

            string lString = lLexer.GetSymbolicString(Formula);

            return lString;
        }

        #endregion
    }
}