﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ModalLogicLib
{
    public class Lexer
    {
        public static string OPEN_BRACE_CHAR = "{";
        public static string CLOSE_BRACE_CHAR = "}";
        public static string PARAMETER_SEPARATOR = ",";

        protected List<ScriptFunction> mSupportedFunctions;
        public static ScriptFunction FUNCTION_NOT = new ScriptFunction("NOT", ScriptFunction.MONARY, "~");
        public static ScriptFunction FUNCTION_AND = new ScriptFunction("AND", ScriptFunction.BINARY, "^");
        public static ScriptFunction FUNCTION_OR = new ScriptFunction("OR", ScriptFunction.BINARY, "v");
        public static ScriptFunction FUNCTION_IMPLIES = new ScriptFunction("IMPLIES", ScriptFunction.BINARY, "=>");
        public static ScriptFunction FUNCTION_NECESSARY = new ScriptFunction("NECESSARY", ScriptFunction.MONARY, "[]");
        public static ScriptFunction FUNCTION_POSSIBLE = new ScriptFunction("POSSIBLE", ScriptFunction.MONARY, "<>");

        public Lexer()
        {
            mSupportedFunctions = new List<ScriptFunction>();
            mSupportedFunctions.Add(FUNCTION_NOT);
            mSupportedFunctions.Add(FUNCTION_AND);
            mSupportedFunctions.Add(FUNCTION_OR);
            mSupportedFunctions.Add(FUNCTION_IMPLIES);
            mSupportedFunctions.Add(FUNCTION_POSSIBLE);
            mSupportedFunctions.Add(FUNCTION_NECESSARY);
        }

        public string NormalizeString(string pScript)
        {
            pScript =  pScript.Replace("(", Lexer.OPEN_BRACE_CHAR).Replace(")", Lexer.CLOSE_BRACE_CHAR);

            if (!IsCorrectOpenCloseTags(pScript))
                throw new Exception(CompilerMessages.ERROR_BAD_OPEN_CLOSE_BRACE);

            return RemoveWhiteSpaces(pScript.ToUpper());
        }

        public int GetOpenBraceCount(string pScript)
        {
            return pScript.Count(f => f.ToString() == Lexer.OPEN_BRACE_CHAR);
        }

        public int GetCloseBraceCount(string pScript)
        {
            return pScript.Count(f => f.ToString() == Lexer.CLOSE_BRACE_CHAR);
        }

        public bool IsCorrectOpenCloseTags(string pScript)
        {
            return GetOpenBraceCount(pScript) == GetCloseBraceCount(pScript);
        }

        public string RemoveWhiteSpaces(string pScript)
        {
            return new String(pScript.Where(x => x != ' ' && x != '\t' && x != '\r' && x != '\n').ToArray());
        }

        public string GetFunctionName(string pScript)
        {
            string lFunctionName = "";

            if (pScript.Length > 1)
            {
                lFunctionName = pScript.Substring(0, pScript.IndexOf(Lexer.OPEN_BRACE_CHAR));
                IsFunctionSupported(lFunctionName);
            }

            return lFunctionName;
        }

        public bool IsFunctionSupported(string pFunction)
        {
            pFunction = pFunction.ToUpper();

            foreach (ScriptFunction tFunction in mSupportedFunctions)
            {
                if (tFunction.Name == pFunction)
                    return true;
            }

            throw new Exception(CompilerMessages.ERROR_UNSUPPORTED_FUNCTION + " '" + pFunction + "'. " + CompilerMessages.ERROR_UNSUPPORTED_FUNCTION_HINT);
        }

        public List<string> GetFunctionParameters(string pScript)
        {
            List<string> lParameters = InternalGetFunctionParameters(pScript);
            ScriptFunction lScriptFunction = GetFunctionData(GetFunctionName(pScript));

            if (lScriptFunction.NumberOfParams == -1)
            {
                if (lParameters.Count < 2)
                    throw new Exception(CompilerMessages.ERROR_INCORRECT_NUMBER_OF_PARAMETERS);
            }
            else
            {
                if( lParameters.Count != lScriptFunction.NumberOfParams)
                    throw new Exception(CompilerMessages.ERROR_INCORRECT_NUMBER_OF_PARAMETERS);
            }
            
            return lParameters;
        }

        private List<string> InternalGetFunctionParameters(string pScript)
        {
            List<string> lParameters = new List<string>();

            pScript = GetFunctionParametersString(pScript);

            int lIndexOfCurrentParameter = 0;
            int lParamStringLenght = 0;
            string tParameter;

            for (int i = 0; i < pScript.Length; i++)
            {
                string lCurrentChar = pScript.ElementAt(i).ToString();

                if (lCurrentChar == Lexer.OPEN_BRACE_CHAR)
                {
                    int lBracesCount = 0;

                    for (; i < pScript.Length; i++)
                    {
                        lCurrentChar = pScript.ElementAt(i).ToString();

                        if (lCurrentChar == Lexer.OPEN_BRACE_CHAR)
                            ++lBracesCount;
                        else if (lCurrentChar == Lexer.CLOSE_BRACE_CHAR)
                            --lBracesCount;

                        if (lBracesCount == 0)
                        {
                            lParamStringLenght = (i+1) - lIndexOfCurrentParameter;
                            tParameter = pScript.Substring(lIndexOfCurrentParameter, lParamStringLenght);

                            AddParameterToList(ref lParameters, tParameter);

                            lIndexOfCurrentParameter = i + 1;
                            break;
                        }
                    }
                }
                else if (lCurrentChar == Lexer.PARAMETER_SEPARATOR)
                {
                    lParamStringLenght = i - lIndexOfCurrentParameter;
                    tParameter = pScript.Substring(lIndexOfCurrentParameter, lParamStringLenght);

                    AddParameterToList(ref lParameters, tParameter);

                    lIndexOfCurrentParameter = i + 1;
                }
            }

            tParameter = pScript.Substring(lIndexOfCurrentParameter);
            AddParameterToList(ref lParameters, tParameter);

            return lParameters;
        }

        private void AddParameterToList(ref List<string> pParameters, string pParameter)
        {
            if (pParameter.Length > 0)
                pParameters.Add(pParameter);
        }

        private string GetFunctionParametersString(string pScript)
        {
            int lIndexOfFirstBrace = pScript.IndexOf(Lexer.OPEN_BRACE_CHAR) + 1;
            int lParamsStringLenght = pScript.Length - lIndexOfFirstBrace - 1;
            string lFunctionParams = pScript.Substring(lIndexOfFirstBrace, lParamsStringLenght);

            return lFunctionParams.Trim();
        }

        private ScriptFunction GetFunctionData(string pFunctionName)
        {
            return mSupportedFunctions.Find(item => item.Name == pFunctionName.ToUpper());
        }

        public int NthIndexOf(string target, string value, int NthOcurrence)
        {
            string lPattern = "((" + value + ").*?){" + NthOcurrence + Lexer.CLOSE_BRACE_CHAR;
            Match m = Regex.Match(target, lPattern);

            if (m.Success)
                return m.Groups[2].Captures[NthOcurrence - 1].Index;
            else
                return -1;
        }

        public string ApplyFunction(ScriptFunction pFunction, string pParam)
        {
            return ApplyFunction(pFunction.Name, pParam);
        }

        private string ApplyFunction(string pFunctionName, string pParam)
        {
            string lResult = string.Empty;

            if (IsFunctionSupported(pFunctionName))
            {
                lResult += pFunctionName;
                lResult += OPEN_BRACE_CHAR;
                lResult += pParam;
                lResult += CLOSE_BRACE_CHAR;
            }

            return lResult;
        }
        
        public string GetSymbolicString(string pScript)
        {
            string lString = "";
            
            if (pScript.Length > 1)
            {
                string lFunctionName = GetFunctionName(pScript);
                List<string> lFunctionParamenters = GetFunctionParameters(pScript);

                foreach (ScriptFunction tScriptFunction in mSupportedFunctions)
                {
                    if (tScriptFunction.Name != lFunctionName)
                        continue;

                    if (tScriptFunction.NumberOfParams == ScriptFunction.MONARY)
                    {
                        lString += tScriptFunction.SymbolicRepresentation;

                        if (lFunctionParamenters[0].Length > 1)
                        {
                            lString += " ";
                            string tSymbolicString = GetSymbolicString(lFunctionParamenters[0]);

                            if (tSymbolicString.Length > 3)
                                tSymbolicString = Lexer.OPEN_BRACE_CHAR + tSymbolicString + Lexer.CLOSE_BRACE_CHAR;

                            lString += tSymbolicString;
                            lString += " ";
                        }
                        else
                        {
                            lString += lFunctionParamenters[0];
                        }
                    }
                    else if (tScriptFunction.NumberOfParams == ScriptFunction.BINARY)
                    {
                        if (lFunctionParamenters[0].Length > 1)
                        {
                            lString += " ";
                            string tSymbolicString = GetSymbolicString(lFunctionParamenters[0]);

                            if (tSymbolicString.Length > 3)
                                tSymbolicString = Lexer.OPEN_BRACE_CHAR + tSymbolicString + Lexer.CLOSE_BRACE_CHAR;

                            lString += tSymbolicString;
                            lString += " ";
                        }
                        else
                        {
                            lString += lFunctionParamenters[0];
                        }

                        lString += tScriptFunction.SymbolicRepresentation;

                        if (lFunctionParamenters[1].Length > 1)
                        {
                            lString += " ";
                            string tSymbolicString = GetSymbolicString(lFunctionParamenters[1]);

                            if (tSymbolicString.Length > 3)
                                tSymbolicString = Lexer.OPEN_BRACE_CHAR + tSymbolicString + Lexer.CLOSE_BRACE_CHAR;

                            lString += tSymbolicString;
                            lString += " ";
                        }
                        else
                        {
                            lString += lFunctionParamenters[1];
                        }
                    }
                }
            }
            else
            {
                lString = pScript;
            }

            return lString;
        }
    }
}
