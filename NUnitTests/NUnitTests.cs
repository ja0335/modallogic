﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

using ModalLogicLib;

namespace NUnitTests
{
    [TestFixture]
    public class NUnitTests
    {
        private Lexer _Lexer;
        private Tableaux_K _Tableaux;

        public NUnitTests()
        {
            _Lexer = new Lexer();
            _Tableaux = new Tableaux_K();
        }

        /// <summary>
        /// ~~A
        /// </summary>
        [Test]
        public void Test_ExpansionRule_NotNot()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{A}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{A}}\n[2]. 1: A	by: [1]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// (A ^ B)
        /// </summary>
        [Test]
        public void Test_ExpansionRule_A_and_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{AND{A,B}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{AND{A,B}}}\nUsed: [2]. 1: AND{A,B}\tby: [1]\n\t[3]. 1: A\tby: [2]\n\t\t[4]. 1: B\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// ~(A ^ B)
        /// </summary>
        [Test]
        public void Test_ExpansionRule_Not_A_and_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{NOT{AND{A,B}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{NOT{AND{A,B}}}}\nUsed: [2]. 1: NOT{AND{A,B}}\tby: [1]\n\t[3]. 1: NOT{A}\tby: [2]\n\t[4]. 1: NOT{B}\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// A v B
        /// </summary>
        [Test]
        public void Test_ExpansionRule_A_or_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{OR{A,B}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{OR{A,B}}}\nUsed: [2]. 1: OR{A,B}\tby: [1]\n\t[3]. 1: A\tby: [2]\n\t[4]. 1: B\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// ~(A v B)
        /// </summary>
        [Test]
        public void Test_ExpansionRule_Not_A_or_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{NOT{OR{A,B}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{NOT{OR{A,B}}}}\nUsed: [2]. 1: NOT{OR{A,B}}\tby: [1]\n\t[3]. 1: NOT{A}\tby: [2]\n\t\t[4]. 1: NOT{B}\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// A => B
        /// </summary>
        [Test]
        public void Test_ExpansionRule_A_implies_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{IMPLIES{A,B}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{IMPLIES{A,B}}}\nUsed: [2]. 1: IMPLIES{A,B}\tby: [1]\n\t[3]. 1: NOT{A}\tby: [2]\n\t[4]. 1: B\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// ~(A => B)
        /// </summary>
        [Test]
        public void Test_ExpansionRule_Not_A_implies_B()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{NOT{IMPLIES{A,B}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{NOT{IMPLIES{A,B}}}}\nUsed: [2]. 1: NOT{IMPLIES{A,B}}\tby: [1]\n\t[3]. 1: A\tby: [2]\n\t\t[4]. 1: NOT{B}\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// ~[]A
        /// </summary>
        [Test]
        public void Test_ExpansionRule_Not_Necessary_A()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{NOT{NECESSARY{A}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{NOT{NECESSARY{A}}}}\nUsed: [2]. 1: NOT{NECESSARY{A}}\tby: [1]\n\tUsed: [3]. 1: POSSIBLE{NOT{A}}\tby: [2]\n\t\t[4]. 2: NOT{A}\tby: [3]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// ~<>A
        /// </summary>
        [Test]
        public void Test_ExpansionRule_Not_Possible_A()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{NOT{POSSIBLE{A}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{NOT{POSSIBLE{A}}}}\nUsed: [2]. 1: NOT{POSSIBLE{A}}\tby: [1]\n\t[3]. 1: NECESSARY{NOT{A}}\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// K-System <>A
        /// </summary>
        [Test]
        public void Test_ExpansionRule_K_System_Possible_A()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{POSSIBLE{A}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{POSSIBLE{A}}}\nUsed: [2]. 1: POSSIBLE{A}\tby: [1]\n\t[3]. 2: A\tby: [2]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        /// <summary>
        /// K-System []A
        /// </summary>
        [Test]
        public void Test_ExpansionRule_K_System_Necessary_A()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{AND{POSSIBLE{A}, NECESSARY{B}}}", out lStepByStep);

            string lExpectedStepByStep = "Used: [1]. 1: NOT{NOT{AND{POSSIBLE{A},NECESSARY{B}}}}\nUsed: [2]. 1: AND{POSSIBLE{A},NECESSARY{B}}\tby: [1]\n\tUsed: [3]. 1: POSSIBLE{A}\tby: [2]\n\t\t[4]. 1: NECESSARY{B}\tby: [2]\n\t\t\t[5]. 2: A\tby: [3]\n\t\t\t\t[6]. 2: B\tby: [4]\n";

            Assert.AreEqual(lExpectedStepByStep, lStepByStep);
        }

        [Test]
        public void K_System_TestValidTheorem_0()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{ IMPLIES{A, B}, IMPLIES{NOT{B}, NOT{A}} }", out lStepByStep);

            Assert.IsTrue(lResult);
        }

        [Test]
        public void K_System_TestValidTheorem_1()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{ NECESSARY{IMPLIES{P,Q}}, IMPLIES{POSSIBLE{P}, POSSIBLE{Q}} }", out lStepByStep);

            Assert.IsTrue(lResult);
        }

        [Test]
        public void K_System_TestValidTheorem_2()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{ NECESSARY{OR{P,Q}}, OR{NECESSARY{P}, POSSIBLE{Q}} }", out lStepByStep);

            Assert.IsTrue(lResult);
        }

        [Test]
        public void K_System_TestValidTheorem_3()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES {AND{NECESSARY { A }, NECESSARY {B}}, NECESSARY{AND{A, B}}}", out lStepByStep);

            Assert.IsTrue(lResult);
        }

        [Test]
        public void K_System_TestValidTheorem_4()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES {OR{NECESSARY { A }, NECESSARY {B}}, NECESSARY{OR{A, B}}}", out lStepByStep);

            Assert.IsTrue(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_0()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{ IMPLIES{A, B}, IMPLIES{NOT{A}, NOT{B}} }", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_1()
        { 
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("NOT{OR{AND{OR{A, B}, OR{NOT{A}, C}}, D}}", out lStepByStep);

            Assert.IsFalse(lResult);            
        }

        [Test]
        public void K_System_TestInvalidTheorem_2()
        { 
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{P}, POSSIBLE{P}}", out lStepByStep);

            Assert.IsFalse(lResult);            
        }

        [Test]
        public void K_System_TestInvalidTheorem_3()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{P}, NECESSARY{NECESSARY{P}}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_4()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NOT{P}, NECESSARY{POSSIBLE{NOT{P}}}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_5()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{P}, P}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_6()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{AND{AND{IMPLIES{NECESSARY{P}, POSSIBLE{P}},IMPLIES{NECESSARY{P}, NECESSARY{NECESSARY{P}}}},IMPLIES{NOT{P}, NECESSARY{POSSIBLE{NOT{P}}}}},IMPLIES{NECESSARY{P}, P}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_7()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{ IMPLIES{NECESSARY{P}, P} }, NECESSARY{P}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_8()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{IMPLIES{P, Q}}, IMPLIES{POSSIBLE{P}, NECESSARY{Q}}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_9()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{IMPLIES{P, Q}}, IMPLIES{POSSIBLE{POSSIBLE{P}}, POSSIBLE{POSSIBLE{Q}}}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_10()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{ NECESSARY{OR{P,NECESSARY{Q}}}, OR{NECESSARY{P}, NECESSARY{Q}} }", out lStepByStep);

            Assert.IsFalse(lResult);
        }

        [Test]
        public void K_System_TestInvalidTheorem_11()
        {
            string lStepByStep;
            bool lResult = _Tableaux.IsTheorem("IMPLIES{NECESSARY{OR{P,Q}},OR{NECESSARY{P},NECESSARY{Q}}}", out lStepByStep);

            Assert.IsFalse(lResult);
        }
    }
}
