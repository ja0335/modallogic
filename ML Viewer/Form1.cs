﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.GraphViewerGdi;

using ModalLogicLib;

namespace ML_Viewer
{
    public partial class Form1 : Form
    {
        Tableaux_K mTableaux;

        public Form1()
        {
            InitializeComponent();
            GenerateGraphView();
        }

        private void GenerateGraphView()
        {
            //create a viewer object 
            GViewer viewer = new GViewer();
            //create a graph object 
            var tree = new PhyloTree();

            textResult.Text = string.Empty;

            if (mTableaux != null)
            {
                try
                {
                    string lStepByStep;
                    string lFormula = formulaTextBox.Text;
                    bool lResult = false;
                    string lStepMsg = string.Empty;

                    if (!stepByStep.Checked)
                        lResult = mTableaux.IsTheorem(lFormula, out lStepByStep);
                    else
                        lStepMsg = mTableaux.Step(lFormula);

                    foreach (ModalLogicLib.Node child in ModalLogicLib.Node.Nodes)
                    {
                        PhyloEdge edge = null;

                        if (child.Left != null)
                            edge = (PhyloEdge) tree.AddEdge(NodeToString(child), NodeToString(child.Left));

                        if (child.Right != null)
                            edge = (PhyloEdge)tree.AddEdge(NodeToString(child), NodeToString(child.Right));

                        if (child.Used)
                            tree.FindNode(NodeToString(child)).Attr.FillColor = Microsoft.Msagl.Drawing.Color.PaleGreen;
                        else if(child.Closed)
                            tree.FindNode(NodeToString(child)).Attr.FillColor = Microsoft.Msagl.Drawing.Color.MediumVioletRed;
                    }

                    //bind the graph to the viewer 
                    viewer.Graph = tree;

                    if (!stepByStep.Checked)
                    {
                        if (lResult)
                            textResult.Text = "The given formula is a theorem";
                        else
                            textResult.Text = "The given formula is not a theorem";
                    }
                    else
                    {
                        textResult.Text = lStepMsg;
                    }
                }
                catch (Exception e)
                {
                    textResult.Text = e.Message;
                }
            }

            //associate the viewer with the form 
            SuspendLayout();
            viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            panelGraphicsView.Controls.Clear();
            panelGraphicsView.Controls.Add(viewer);
            ResumeLayout();
        }

        private string NodeToString(ModalLogicLib.Node pNode)
        {
            string lString = "";

            if (pNode.Used)
                lString += "Used: ";

            lString += "[" + pNode.Index.ToString() + "]. " + pNode.Label.ToString() + ": ";

            if (getSymbolicResult.Checked)
                lString += pNode.GetSymbolicString();
            else
                lString += pNode.Formula;

            if (pNode.InferedBy != null)
                lString += "\t  " + "by: [" + pNode.InferedBy.Index.ToString() + "]";

            if (pNode.Closed)
                lString += " X";

            return lString;
        }

        private bool FormCheck()
        {
            bool isFormOk = true;
            string errorRecopilation = string.Empty;

            if (formulaTextBox.Text == string.Empty)
            {
                errorRecopilation = "You have not introduced a formula.";
                isFormOk = false;
            }

            if (!isFormOk)
                MessageBox.Show(errorRecopilation);

            return isFormOk;
        }

        private void buttonK_Click(object sender, EventArgs e)
        {
            if (FormCheck())
            {
                panelGraphicsView.Controls.Clear();

                mTableaux = new Tableaux_K();

                GenerateGraphView();
            }
        }

        private void buttonS2_Click(object sender, EventArgs e)
        {
            if (FormCheck())
            {
                panelGraphicsView.Controls.Clear();

                mTableaux = new Tableaux_S2();

                GenerateGraphView();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mTableaux = new Tableaux_K();

            mTableaux.ResetStepByStep();

            mTableaux = null;

            stepByStep.Checked = false;

            textResult.Text = string.Empty;

            GenerateGraphView();
        }
        
        private void aboutModalLogicProverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_About lFormAbout = new Form_About();

            lFormAbout.Show();
        }
    }
}
