﻿namespace ML_Viewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.formulaTextBox = new System.Windows.Forms.TextBox();
            this.panelGraphicsView = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.stepByStep = new System.Windows.Forms.CheckBox();
            this.textResult = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.getSymbolicResult = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutModalLogicProverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.buttonK_Click);
            // 
            // formulaTextBox
            // 
            resources.ApplyResources(this.formulaTextBox, "formulaTextBox");
            this.formulaTextBox.Name = "formulaTextBox";
            // 
            // panelGraphicsView
            // 
            resources.ApplyResources(this.panelGraphicsView, "panelGraphicsView");
            this.panelGraphicsView.Name = "panelGraphicsView";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.buttonS2_Click);
            // 
            // stepByStep
            // 
            resources.ApplyResources(this.stepByStep, "stepByStep");
            this.stepByStep.Name = "stepByStep";
            this.stepByStep.UseVisualStyleBackColor = true;
            // 
            // textResult
            // 
            resources.ApplyResources(this.textResult, "textResult");
            this.textResult.Name = "textResult";
            this.textResult.ReadOnly = true;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // getSymbolicResult
            // 
            resources.ApplyResources(this.getSymbolicResult, "getSymbolicResult");
            this.getSymbolicResult.Checked = true;
            this.getSymbolicResult.CheckState = System.Windows.Forms.CheckState.Checked;
            this.getSymbolicResult.Name = "getSymbolicResult";
            this.getSymbolicResult.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutModalLogicProverToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // aboutModalLogicProverToolStripMenuItem
            // 
            this.aboutModalLogicProverToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.aboutModalLogicProverToolStripMenuItem.Name = "aboutModalLogicProverToolStripMenuItem";
            resources.ApplyResources(this.aboutModalLogicProverToolStripMenuItem, "aboutModalLogicProverToolStripMenuItem");
            this.aboutModalLogicProverToolStripMenuItem.Click += new System.EventHandler(this.aboutModalLogicProverToolStripMenuItem_Click);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.getSymbolicResult);
            this.panel1.Controls.Add(this.formulaTextBox);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.stepByStep);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Name = "panel1";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textResult);
            this.Controls.Add(this.panelGraphicsView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox formulaTextBox;
        private System.Windows.Forms.Panel panelGraphicsView;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox stepByStep;
        private System.Windows.Forms.TextBox textResult;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox getSymbolicResult;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutModalLogicProverToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
    }
}

