﻿////////////////////////////////////////////////////////////
//
// MODAL LOGIC AUTOMATIC PROVER K SYSTEMS
// Copyright (C) 2014 
//      Luz Amparo Carranza (luza.carranzag@konradlorenz.edu.co)
//      Juan Camilo Acosta Arango (ja0335@hotmail.com | juanc.acostaa@konradlorenz.edu.co)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModalLogicLib;

namespace ModalLogicApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Automatic K / S2 - Theorem prover *****...");

            Tableaux_K lTableaux = GetSystem();
            string lStepByStep;
            
            while (true)
            {
                try
                {
                    Console.WriteLine("\n\nInsert the formula to test...");

                    string lFormula = Console.ReadLine();

                    bool lResult = lTableaux.IsTheorem(lFormula, out lStepByStep);

                    Console.WriteLine(lStepByStep);
                    System.IO.File.WriteAllText(@"result.txt", lStepByStep);

                    if (lResult)
                        Console.WriteLine("The given formula is a theorem");
                    else
                        Console.WriteLine("The given formula is not a theorem");

                    Console.WriteLine("\n\n");
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static Tableaux_K GetSystem()
        {
            Tableaux_K lTableaux = null;

            while (lTableaux == null)
            { 
                Console.WriteLine("Select system (K / S2):");

                string lSystem = Console.ReadLine();

                if (lSystem.ToUpper() == "K")
                {
                    lTableaux = new Tableaux_K();
                    Console.WriteLine(":::::::::::::::::Working over K system:::::::::::::::::");
                }
                else if (lSystem.ToUpper() == "S2")
                {
                    lTableaux = new Tableaux_S2();
                    Console.WriteLine(":::::::::::::::::Working over S2 system:::::::::::::::::");
                }
                else
                    Console.WriteLine("Only K / S2 systems supported...");
            }

            return lTableaux;
        }
    }
}
